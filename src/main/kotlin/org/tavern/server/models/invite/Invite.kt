package org.tavern.server.models.invite

data class Invite (
        var id: String = "",
        var key: String = "",
        var count: Int = 0,
        var used: Int = 0
)