package org.tavern.server.models.chat

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Message(
        var meta: Meta,
        var messages: List<Messages>
        )