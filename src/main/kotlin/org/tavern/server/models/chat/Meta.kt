package org.tavern.server.models.chat

import org.tavern.server.models.profile.Profile

data class Meta(
        var users: List<Profile> = listOf()
)