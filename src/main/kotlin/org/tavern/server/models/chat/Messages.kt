package org.tavern.server.models.chat

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Messages(
        var id: String? = null,
        var date: String? = null,
        var userId: String,
        var userName: String,
        var avatar: String,
        var text: String
)