package org.tavern.server.models.search

data class Search(
        var list: List<Entity>,
        var count: Int
)