package org.tavern.server.models.search

data class Entity(
        var id: String,
        var title: String,
        var cover: String
)