package org.tavern.server.models.meta

import org.tavern.server.models.entity.Item

data class Content (
        var list: List<Item> = mutableListOf(),
        var count: Long = 0
)