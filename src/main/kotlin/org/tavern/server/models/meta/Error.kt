package org.tavern.server.models.meta

data class Error (
        var status: Boolean = false,
        var message: String = ""
)