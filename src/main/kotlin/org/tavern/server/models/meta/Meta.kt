package org.tavern.server.models.meta

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Meta <T> (
        var error: Error,
        var content: T? = null
)