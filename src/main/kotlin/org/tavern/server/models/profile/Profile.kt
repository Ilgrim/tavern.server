package org.tavern.server.models.profile

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Profile (
        var id: String? = null,
        var login: String = "",
        var displayName: String = "",
        var password: String = "",
        var avatar: String = "",
        var invite: String = "",
        var token: String = ""
) {
    override fun equals(other: Any?): Boolean {
        if (other is Profile) {
            return this.id == other.id
        }
        return false
    }
}