package org.tavern.server.models.entity

import com.fasterxml.jackson.annotation.JsonInclude


@JsonInclude(JsonInclude.Include.NON_NULL)
data class Author (
        var id: String,
        var authorName: String
)