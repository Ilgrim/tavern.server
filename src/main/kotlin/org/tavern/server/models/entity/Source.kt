package org.tavern.server.models.entity

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Source (
        var id: String,
        var entityId: String,
        var source: String
)