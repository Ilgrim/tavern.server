package org.tavern.server.models.entity

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class AlternativeName (
        var id: String? = null,
        var entityId: String? = null,
        var alternativeName: String
)