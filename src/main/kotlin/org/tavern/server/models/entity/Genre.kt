package org.tavern.server.models.entity

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Genre (
        var id: String,
        var genreName: String
)