package org.tavern.server.models.entity

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Item (
        var id: String = "",
        var title: String = "",
        var description: String = "",
        var cover: String = "",
        var release: Boolean = false,
        var strRelease: String =  "",
        var strTranslate: String = "",
        var translate: Boolean = false,
        var entity_type: Type = Type("", ""),
        var alternative_names_entity: List<AlternativeName> = listOf(),
        var entity_authors: List<Author> = listOf(),
        var entity_genres: List<Genre> = listOf(),
        var entity_sources: List<Source> = listOf()
)