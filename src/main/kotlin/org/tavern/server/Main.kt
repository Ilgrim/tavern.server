package org.tavern.server

import org.jooby.*
import org.jooby.handlers.CorsHandler
import org.jooby.json.Jackson
import org.tavern.server.routes.Auth
import org.tavern.server.routes.Catalog
import org.tavern.server.websockets.Chat
import org.tavern.server.websockets.ChatApi
import java.nio.charset.Charset


class App : Kooby ({
    //use("*", CorsHandler())
    use(Jackson())
    use(Auth::class)
    use(Catalog::class)
    use(ChatApi::class)

    ws(Chat::class.java)

    after { _, rsp, result ->
        rsp.charset(Charset.forName("UTF-8"))
        rsp.header("charset", "utf-8")
        result
    }

})

fun main(args:Array<String>) {
    run(::App, *args)
}
