package org.tavern.server.database

import com.rethinkdb.RethinkDB.r
import com.rethinkdb.net.Connection
import com.rethinkdb.net.Cursor

/*
* Возвращает количество записей из таблицы
*
* @property table название таблицы
* @property getConnection подключение к базе данных
* @return Long
* */
fun getCount(table: String, connection: Connection): Long {
    return r.table(table).count()
            .run<Long>(connection)
}