package org.tavern.server.database.queries

import com.rethinkdb.RethinkDB
import com.rethinkdb.net.Connection
import org.tavern.server.json.jsonMapper
import org.tavern.server.models.chat.Message
import org.tavern.server.models.chat.Messages
import org.tavern.server.models.chat.Meta

fun insertChatMsg(json: String, connection: Connection) {
    RethinkDB.r.table("chat")
            .insert(RethinkDB.r.json(json))
            .run<Any>(connection)
}

fun getLastChatMsg(connection: Connection): Message {
    val count: Long =  RethinkDB.r.table("chat").count()
            .run(connection)

    if (count == 0L) return Message(Meta(), listOf())

    val json: String =  RethinkDB.r.table("chat")
            .orderBy(RethinkDB.r.asc("date"))
            .skip(if(count < 20) {0} else {count - 20})
            .coerceTo("array")
            .toJson()
            .run(connection)

    val msgs = jsonMapper().readValue(json, Array<Messages>::class.java)
    val users = getUsersOnline(connection)

    return Message(Meta(users), msgs.toList())
}