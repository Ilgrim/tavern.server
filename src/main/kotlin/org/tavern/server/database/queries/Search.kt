package org.tavern.server.database.queries

import com.rethinkdb.RethinkDB.r
import com.rethinkdb.net.Connection
import org.tavern.server.json.jsonMapper
import org.tavern.server.models.search.Entity
import org.tavern.server.models.search.Search

fun search(connection: Connection, query: String, authorId: String, typeId: String, skip: Int = 0, limit: Int = 20): Search {
    val byName = if (query == "") {listOf<String>()} else {searchByName(connection, query)}
    val byAuthor = if (authorId == "") {listOf<String>()} else {searchByAuthor(connection, authorId)}
    val byType = if (typeId == "") {listOf<String>()} else {searchByType(connection, typeId)}

    println(typeId)
    var list: List<String?> = mutableListOf()

    byName.forEach {
        list = list.plus(it)
    }
    byAuthor.forEach {
        list = list.plus(it)
    }
    byType.forEach {
        list = list.plus(it)
    }
    list = list.distinct()

    val entities = searchStepThree(connection, list, skip, limit)

    return Search(entities, list.size)
}
fun searchByName(connection: Connection, query: String): List<String?> {
    var list = searchStepOne(connection, query)
    val listTwo = searchStepTwo(connection, query)

    listTwo.forEach {
        list = list.plus(it)
    }
    return list.distinct()
}
fun searchStepOne(connection: Connection, query: String): List<String?> {
    val response: List<HashMap<String, String>> = r.table("alternative_names_entity")
            .pluck("entityId", "alternativeName")
            .filter{q -> q.g("alternativeName").match("(?i)$query")}
            .coerceTo("array")
            .without("alternativeName")
            .run(connection)

    var list: List<String?> = mutableListOf()
    response.forEach {
        list = list.plus(it["entityId"])
    }

    return list
}
fun searchStepTwo(connection: Connection, query: String): List<String?> {
    val response: List<HashMap<String, String>> = r.table("entity")
            .pluck("id", "title")
            .filter{q -> q.g("title").match("(?i)$query")}
            .coerceTo("array")
            .without("title")
            .run(connection)

    var list: List<String?> = mutableListOf()
    response.forEach {
        list = list.plus(it["id"])
    }

    return list
}

fun searchByAuthor(connection: Connection, authorId: String): List<String?> {
    return searchByTag(connection, "entity_to_authors", "authorId", authorId)
}

fun searchByType(connection: Connection, typeId: String): List<String?> {
    return searchByTag(connection, "entity_to_type", "typeId", typeId)
}


fun searchByTag(connection: Connection, table: String, tag: String, tagId: String): List<String?> {
    val response: List<HashMap<String, String>> = r.table(table)
            .filter{r.hashMap(tag, tagId)}
            .coerceTo("array")
            .run(connection)

    var list: List<String?> = mutableListOf()
    response.forEach {
        list = list.plus(it["entityId"])
    }

    return list
}

fun searchStepThree(connection: Connection, idList: List<String?>, skip: Int = 0, limit: Int = 20): List<Entity> {
    val json: String = r.table("entity")
            .getAll(r.args(idList))
            .pluck("id", "title", "cover")
            .skip(skip)
            .limit(limit)
            .coerceTo("array")
            .toJson()
            .run(connection)

    val entities: Array<Entity> = jsonMapper().readValue(json, Array<Entity>::class.java)

    return entities.toList()
}