package org.tavern.server.database.queries

import com.rethinkdb.RethinkDB
import com.rethinkdb.net.Connection
import org.tavern.server.json.jsonMapper
import org.tavern.server.models.invite.Invite
import org.tavern.server.models.meta.Error
import org.tavern.server.models.profile.Profile

fun updateInvite(inviteId: String, newUsed: Int, connection: Connection) {
    RethinkDB.r.table("invitations").get(inviteId)
            .update(RethinkDB.r.hashMap("used", newUsed))
            .run<Unit>(connection)
}

fun getInvite(invite: String, connection: Connection): Pair<Error, Invite> {
    val json: String = RethinkDB.r.table("invitations")
            .filter(RethinkDB.r.hashMap("key", invite))
            .coerceTo("array")
            .toJson()
            .run(connection)
    val invites: List<Invite> = jsonMapper().readValue(json, Array<Invite>::class.java).toList()

    return if (invites.isEmpty()) {
        val error = Error(true, "Ключ не найден")
        Pair(error, Invite())
    } else {
        val error = Error(false)
        Pair(error, invites[0])
    }
}

fun checkLoginName(login: String, connection: Connection): Boolean {
    val json: String = RethinkDB.r.table("profiles")
            .filter(RethinkDB.r.hashMap("login", login))
            .coerceTo("array")
            .toJson()
            .run(connection)
    val profiles: List<Profile> = jsonMapper().readValue(json, Array<Profile>::class.java).toList()

    return !profiles.isEmpty()
}

fun checkLoginPassword(login: String, password: String, connection: Connection): List<Profile> {
    val json: String = RethinkDB.r.table("profiles")
            .filter(RethinkDB.r.hashMap("login", login))
            .filter(RethinkDB.r.hashMap("password", password))
            .coerceTo("array")
            .toJson()
            .run(connection)

    return jsonMapper().readValue(json, Array<Profile>::class.java).toList()
}

fun getProfile(id: String?, connection: Connection): Pair<Error, Profile> {
    val json: String? = RethinkDB.r.table("profiles")
            .get(id)
            .toJson()
            .run(connection)

    return if (json.equals("null")) {
        val error = Error(true, "profile not found")
        Pair(error, Profile())
    } else {
        val error = Error(false)
        val profile = jsonMapper().readValue(json, Profile::class.java)
        Pair(error, profile)
    }
}

fun getUsersOnline(connection: Connection): List<Profile> {
    val json: String? = RethinkDB.r.table("profiles")
            .coerceTo("array")
            .toJson()
            .run(connection)

    val profiles = jsonMapper().readValue(json, Array<Profile>::class.java)

    return profiles.filter { it.token != "" }
}



fun updateProfileToken(profileId: String?, token: String, connection: Connection) {
    RethinkDB.r.table("profiles").get(profileId)
            .update(RethinkDB.r.hashMap("token", token))
            .run<Unit>(connection)
}

fun insertProfileWithId(json: String, connection: Connection): String {
    return RethinkDB.r.table("profiles")
            .insert(RethinkDB.r.json(json))
            .getField("generated_keys").nth(0)
            .run<String>(connection)
}
