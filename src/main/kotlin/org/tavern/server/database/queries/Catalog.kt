package org.tavern.server.database.queries

import com.rethinkdb.RethinkDB
import com.rethinkdb.net.Connection
import org.tavern.server.json.jsonMapper
import org.tavern.server.models.entity.Item

/*
* Возвращает данные из таблиицы Entity
*
* @property skip указывает сколько строк пропустить
* @property limit указывает на ограничение количества данных
* @property customFields массив полей таблицы. Доступные поля смотреть в базе
* @property getConnection подключение к базе данных
* @return Cursor<HashMap<String, Any>>
* */
fun getEntity(skip: Int = 0, limit: Int = 20, vararg customFields: String, connection: Connection): List<Item> {
    val fields = if (customFields.isEmpty()) arrayOf("id", "title", "cover", "release", "translate") else customFields
    val json: String = RethinkDB.r.table("entity")
            .pluck(fields)
            .skip(skip)
            .limit(limit)
            .coerceTo("array")
            .toJson()
            .run(connection)

    val entities: Array<Item> = jsonMapper().readValue(json, Array<Item>::class.java)

    return entities.toList()
}

fun getEntityItem(entityId: String, connection: Connection): Item {
    val json: String = RethinkDB.r.table("entity")
            .get(entityId)
            .merge{ row -> RethinkDB.r.hashMap("alternative_names_entity", RethinkDB.r.table("alternative_names_entity")
                    .filter(RethinkDB.r.hashMap("entityId", row.g("id"))).coerceTo("array"))}
            .merge{ row -> RethinkDB.r.hashMap("entity_sources", RethinkDB.r.table("entity_sources")
                    .filter(RethinkDB.r.hashMap("entityId", row.g("id"))).coerceTo("array"))}
            .merge{ row -> RethinkDB.r.hashMap("entity_to_authors", RethinkDB.r.table("entity_to_authors")
                    .filter(RethinkDB.r.hashMap("entityId", row.g("id"))).coerceTo("array"))}
            .merge{ row -> RethinkDB.r.hashMap("entity_to_genres", RethinkDB.r.table("entity_to_genres")
                    .filter(RethinkDB.r.hashMap("entityId", row.g("id"))).coerceTo("array"))}
            .merge{ row -> RethinkDB.r.hashMap("entity_to_type", RethinkDB.r.table("entity_to_type")
                    .filter(RethinkDB.r.hashMap("entityId", row.g("id"))).coerceTo("array"))}
            .merge{ row -> RethinkDB.r.hashMap("entity_authors", RethinkDB.r.table("entity_authors")
                    .filter { ea -> row.g("entity_to_authors").g("authorId").contains(ea.g("id")) }
                    .coerceTo("array"))}
            .merge{ row -> RethinkDB.r.hashMap("entity_genres", RethinkDB.r.table("entity_genres")
                    .filter { eg -> row.g("entity_to_genres").g("genreId").contains(eg.g("id")) }
                    .coerceTo("array"))}
            .merge{ row -> RethinkDB.r.hashMap("entity_type", RethinkDB.r.table("entity_type")
                    .filter { et -> row.g("entity_to_type").g("typeId").contains(et.g("id")) }
                    .coerceTo("array").nth(0))}
            .without("id", "url", "entity_to_authors", "entity_to_genres", "entity_to_type")
            .toJson()
            .run(connection)

    return jsonMapper().readValue(json, Item::class.java)
}