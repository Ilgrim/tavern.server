package org.tavern.server.database

import com.rethinkdb.RethinkDB
import com.rethinkdb.net.Connection

fun getConnection(): Connection {
    return RethinkDB.r.connection()
            .db("tavern").hostname("localhost")
            .port(28015).connect()
}