package org.tavern.server.websockets

import com.rethinkdb.net.Connection
import org.jooby.Request
import org.jooby.Response
import org.jooby.mvc.GET
import org.jooby.mvc.Path
import org.junit.Before
import org.tavern.server.database.getConnection
import org.tavern.server.database.queries.getLastChatMsg
import org.tavern.server.models.chat.Message
import org.tavern.server.models.meta.Error
import org.tavern.server.models.meta.Meta

class ChatApi {
    private val connection: Connection = getConnection()

    @Before
    @Path("/chat/**")
    fun forChat(request: Request, response: Response) {
        val session = request.session()
        if (!session.get("token").isSet) {
            val data: Meta<Any> = Meta(error = Error(true, "Not found!"))
            response.status(404)
                    .type("application/json;charset=UTF-8")
                    .send(data)
        }
    }

    @GET
    @Path("/chat/api")
    fun chatApi(): Message {
        return getLastChatMsg(connection)
    }
}