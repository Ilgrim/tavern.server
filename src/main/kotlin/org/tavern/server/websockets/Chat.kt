package org.tavern.server.websockets

import com.google.inject.Inject
import com.rethinkdb.net.Connection
import org.jooby.WebSocket
import org.jooby.mvc.Path
import org.tavern.server.database.getConnection
import org.tavern.server.database.queries.getUsersOnline
import org.tavern.server.database.queries.insertChatMsg
import org.tavern.server.json.jsonMapper
import org.tavern.server.models.chat.Message
import org.tavern.server.models.chat.Messages
import org.tavern.server.models.chat.Meta
import java.text.SimpleDateFormat
import java.util.*

@Path("/chat")
class Chat @Inject constructor(private val ws: WebSocket) : WebSocket.OnMessage<String>, WebSocket.OnOpen1 {

    private val connection: Connection = getConnection()

    override fun onOpen(ws: WebSocket?) {

    }

    override fun onMessage(message: String) {
        val msg = jsonMapper().readValue(message, Messages::class.java)

        if (msg.text == "ping") return

        msg.date = getDate()
        val json = jsonMapper().writeValueAsString(msg)
        insertChatMsg(json, connection)


        val users = getUsersOnline(connection)
        val message = Message(Meta(users), listOf(msg))
        val jsonComplete = jsonMapper().writeValueAsString(message)
        ws.broadcast(jsonComplete)
    }

    fun getDate(): String {
        val dateFormat = SimpleDateFormat("yyyy.MM.dd HH:mm")
        val date = Date()
        return dateFormat.format(date)
    }
}