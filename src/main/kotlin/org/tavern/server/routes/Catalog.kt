package org.tavern.server.routes

import com.rethinkdb.net.Connection
import org.jooby.Request
import org.jooby.mvc.GET
import org.jooby.mvc.Path
import org.tavern.server.database.*
import org.tavern.server.database.queries.getEntity
import org.tavern.server.database.queries.getEntityItem
import org.tavern.server.database.queries.search
import org.tavern.server.models.entity.Item
import org.tavern.server.models.meta.Content
import org.tavern.server.models.meta.Error
import org.tavern.server.models.meta.Meta
import org.tavern.server.models.search.Search

class Catalog {
    private val connection: Connection = getConnection()

//    @Before
//    @Path("/manga/**")
//    fun forManga(request: Request, response: Response) {
//        checkAuth(request, response)
//    }
//
//    fun checkAuth(request: Request, response: Response) {
//        val session = request.session()
//        if (!session.get("token").isSet) {
//            val data: Meta<Any> = Meta(error = Error(true, "Not found!"))
//            response.status(404)
//                    .type("application/json;charset=UTF-8")
//                    .send(data)
//        }
//    }

    @GET
    @Path("/manga/search")
    fun search(req: Request, skip: Int): Meta<Search> {

        val query = if(req.param("query").isSet){req.param("query").value()}else{""}
        val authorId = if(req.param("authorId").isSet){req.param("authorId").value()}else{""}
        val typeId = if(req.param("typeId").isSet){req.param("typeId").value()}else{""}

        val result = search(connection, query, authorId, typeId, skip)
        return Meta(Error(), content = result)
    }

    @GET
    @Path("/manga")
    fun manga(skip: Int): Meta<Content> {
        val list: List<Item> = getEntity(skip = skip, connection = connection)
        val count: Long = getCount("entity", connection)

        list.forEach {
            if (it.release)
                it.strRelease = "Выпуск завершен"
            else
                it.strRelease = "Выпуск продолжается"

            if (it.translate)
                it.strTranslate = "Перевод завершен"
            else
                it.strTranslate = "Перевод продолжается"
        }

        return Meta(Error(), content = Content(list, count))
    }

    @GET
    @Path("/manga/{id:[\\w]{8}-[\\w]{4}-[\\w]{4}-[\\w]{4}-[\\w]{12}}")
    fun card(request: Request): Meta<Item> {
        val id = request.param("id").value()
        val item: Item = getEntityItem(id, connection)
        if (item.release)
            item.strRelease = "Выпуск завершен"
        else
            item.strRelease = "Выпуск продолжается"

        if (item.translate)
            item.strTranslate = "Перевод завершен"
        else
            item.strTranslate = "Перевод продолжается"

        return Meta(Error(), content = item)
    }
}