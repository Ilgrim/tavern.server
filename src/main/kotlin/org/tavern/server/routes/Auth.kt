package org.tavern.server.routes

import com.rethinkdb.net.Connection
import org.jooby.Request
import org.jooby.Response
import org.jooby.mvc.GET
import org.jooby.mvc.POST
import org.jooby.mvc.Path
import org.tavern.server.database.*
import org.tavern.server.database.queries.*
import org.tavern.server.models.meta.Error
import org.tavern.server.json.jsonMapper
import org.tavern.server.models.invite.Invite
import org.tavern.server.models.meta.Meta
import org.tavern.server.models.profile.Profile
import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.util.*


class Auth {
    private val connection: Connection = getConnection()

    @POST
    @Path("/registration")
    fun registration(request: Request, response: Response): Meta<Profile> {
        val newUser: Profile = request.params(Profile::class.java)

        val invite: Invite = regCheck(newUser, response)

        val profileId: String = insertProfileWithId(jsonMapper().writeValueAsString(newUser), connection)
        invite.used += 1
        updateInvite(invite.id, invite.used, connection)

        newUser.id = profileId
        newUser.password = ""

        return Meta(Error(), content = newUser)
    }

    @POST
    @Path("/login")
    fun login(request: Request, response: Response): Meta<Profile> {
        val user: Profile = request.params(Profile::class.java)
        loginIsEmpty(user, response)
        passwordIsEmpty(user, response)
        val result = checkLoginPassword(user.login, user.password, connection)

        checkAuth(result, response)

        val session = request.session()
        val token = makeToken(user)
        session.set("token", token)
        updateProfileToken(result[0].id, token, connection)
        result[0].token = token
        return Meta(Error(false), result[0])
    }

    @GET
    @Path("/logout")
    fun logout(id: String) {
        updateProfileToken(id, "", connection)
    }

    private fun inviteIsEmpty(user: Profile, response: Response) {
        if (user.invite.isEmpty()) {
            val data: Meta<Profile> = Meta(error = Error(true, "Заполните поле Ключ"))
            response.status(401)
                    .type("application/json;charset=UTF-8")
                    .send(data)
        }
    }

    private fun checkInvite(user: Profile, response: Response): Invite {
        val resInvite: Pair<Error, Invite> = getInvite(user.invite, connection)
        if (resInvite.first.status) {
            val data: Meta<Profile> = Meta(error = resInvite.first)
            response.status(401)
                    .type("application/json;charset=UTF-8")
                    .send(data)
        }

        return resInvite.second
    }

    private fun checkUsedInvite(invite: Invite, response: Response) {
        if (invite.used == invite.count) {
            val data: Meta<Profile> = Meta(error = Error(true, "Инвайт израсходован"))
            response.status(401)
                    .type("application/json;charset=UTF-8")
                    .send(data)
        }
    }

    private fun loginIsEmpty(user: Profile, response: Response) {
        if (user.login.isEmpty()) {
            val data: Meta<Profile> = Meta(error = Error(true, "Заполните поле Логин"))
            response.status(401)
                    .type("application/json;charset=UTF-8")
                    .send(data)
        }
    }

    private fun passwordIsEmpty(user: Profile, response: Response) {
        if (user.password.isEmpty()) {
            val data: Meta<Profile> = Meta(error = Error(true, "Заполните поле Пароль"))
            response.status(401)
                    .type("application/json;charset=UTF-8")
                    .send(data)
        }
    }

    private fun checkExistLogin(user: Profile, response: Response) {
        val checkLogin: Boolean = checkLoginName(user.login, connection)

        if (checkLogin) {
            val data: Meta<Profile> = Meta(error = Error(true, "Такой логин уже используется"))
            response.status(401)
                    .type("application/json;charset=UTF-8")
                    .send(data)
        }
    }

    private fun regCheck(user: Profile, response: Response): Invite {
        inviteIsEmpty(user, response)

        val invite: Invite = checkInvite(user, response)

        checkUsedInvite(invite, response)

        loginIsEmpty(user, response)
        passwordIsEmpty(user, response)

        checkExistLogin(user, response)

        return invite
    }

    private fun checkAuth(result: List<Profile>, response: Response) {
        if (result.isEmpty()) {
            val data: Meta<Any> = Meta(error = Error(true, "Не верный логин или пароль"))
            response.status(401)
                    .type("application/json;charset=UTF-8")
                    .send(data)
        }
    }

    private fun makeToken(user: Profile): String {
        val code = "${user.login} ${user.password} 1q2w3e4r ${System.nanoTime()}"
        val digest = MessageDigest.getInstance("SHA-512")
        val hash = digest.digest(code.toByteArray(StandardCharsets.UTF_8))
        return Base64.getEncoder().encodeToString(hash)
    }
}