package org.tavern.server.json

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule

fun jsonMapper(): ObjectMapper {
    return ObjectMapper().registerModule(KotlinModule())
}